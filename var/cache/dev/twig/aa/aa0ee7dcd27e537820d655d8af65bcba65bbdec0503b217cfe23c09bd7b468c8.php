<?php

/* @WebProfiler/Profiler/open.html.twig */
class __TwigTemplate_91696a6e0cc9aad2b2a6ffbbf8651f1b29e3cbc2e48bd5dda9ee8ccabf53e928 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@WebProfiler/Profiler/base.html.twig", "@WebProfiler/Profiler/open.html.twig", 1);
        $this->blocks = array(
            'head' => array($this, 'block_head'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@WebProfiler/Profiler/base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_6767e5da4715081c5b455bf4c60d97c90548a8898a8530fd5ffbdfb7019bbbd0 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_6767e5da4715081c5b455bf4c60d97c90548a8898a8530fd5ffbdfb7019bbbd0->enter($__internal_6767e5da4715081c5b455bf4c60d97c90548a8898a8530fd5ffbdfb7019bbbd0_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@WebProfiler/Profiler/open.html.twig"));

        $__internal_82e81c6b2ec27ab05ef7bee50c2674d042ac6d7d4433700fb18243410a55cb77 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_82e81c6b2ec27ab05ef7bee50c2674d042ac6d7d4433700fb18243410a55cb77->enter($__internal_82e81c6b2ec27ab05ef7bee50c2674d042ac6d7d4433700fb18243410a55cb77_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@WebProfiler/Profiler/open.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_6767e5da4715081c5b455bf4c60d97c90548a8898a8530fd5ffbdfb7019bbbd0->leave($__internal_6767e5da4715081c5b455bf4c60d97c90548a8898a8530fd5ffbdfb7019bbbd0_prof);

        
        $__internal_82e81c6b2ec27ab05ef7bee50c2674d042ac6d7d4433700fb18243410a55cb77->leave($__internal_82e81c6b2ec27ab05ef7bee50c2674d042ac6d7d4433700fb18243410a55cb77_prof);

    }

    // line 3
    public function block_head($context, array $blocks = array())
    {
        $__internal_8ebc732f2dc317d734cbaffe0091ca1cab915906bf38d5c75cb77761861108e8 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_8ebc732f2dc317d734cbaffe0091ca1cab915906bf38d5c75cb77761861108e8->enter($__internal_8ebc732f2dc317d734cbaffe0091ca1cab915906bf38d5c75cb77761861108e8_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "head"));

        $__internal_b87518b85923f59a773012f98981048377715cfff369f009e301f24ac95b3caf = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_b87518b85923f59a773012f98981048377715cfff369f009e301f24ac95b3caf->enter($__internal_b87518b85923f59a773012f98981048377715cfff369f009e301f24ac95b3caf_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "head"));

        // line 4
        echo "    <style>
        ";
        // line 5
        echo twig_include($this->env, $context, "@WebProfiler/Profiler/open.css.twig");
        echo "
    </style>
";
        
        $__internal_b87518b85923f59a773012f98981048377715cfff369f009e301f24ac95b3caf->leave($__internal_b87518b85923f59a773012f98981048377715cfff369f009e301f24ac95b3caf_prof);

        
        $__internal_8ebc732f2dc317d734cbaffe0091ca1cab915906bf38d5c75cb77761861108e8->leave($__internal_8ebc732f2dc317d734cbaffe0091ca1cab915906bf38d5c75cb77761861108e8_prof);

    }

    // line 9
    public function block_body($context, array $blocks = array())
    {
        $__internal_92dd56c20888f1dfbc1410314eb9665321a60f7aeb774727be846e0eaaac1a7c = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_92dd56c20888f1dfbc1410314eb9665321a60f7aeb774727be846e0eaaac1a7c->enter($__internal_92dd56c20888f1dfbc1410314eb9665321a60f7aeb774727be846e0eaaac1a7c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_4b972525da5e194241ca083ffb432de2175cff34990edafccbd2e2129b4dd7f3 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_4b972525da5e194241ca083ffb432de2175cff34990edafccbd2e2129b4dd7f3->enter($__internal_4b972525da5e194241ca083ffb432de2175cff34990edafccbd2e2129b4dd7f3_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 10
        echo "<div class=\"header\">
    <h1>";
        // line 11
        echo twig_escape_filter($this->env, ($context["file"] ?? $this->getContext($context, "file")), "html", null, true);
        echo " <small>line ";
        echo twig_escape_filter($this->env, ($context["line"] ?? $this->getContext($context, "line")), "html", null, true);
        echo "</small></h1>
    <a class=\"doc\" href=\"https://symfony.com/doc/";
        // line 12
        echo twig_escape_filter($this->env, twig_constant("Symfony\\Component\\HttpKernel\\Kernel::VERSION"), "html", null, true);
        echo "/reference/configuration/framework.html#ide\" rel=\"help\">Open in your IDE?</a>
</div>
<div class=\"source\">
    ";
        // line 15
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\CodeExtension')->fileExcerpt(($context["filename"] ?? $this->getContext($context, "filename")), ($context["line"] ?? $this->getContext($context, "line")),  -1);
        echo "
</div>
";
        
        $__internal_4b972525da5e194241ca083ffb432de2175cff34990edafccbd2e2129b4dd7f3->leave($__internal_4b972525da5e194241ca083ffb432de2175cff34990edafccbd2e2129b4dd7f3_prof);

        
        $__internal_92dd56c20888f1dfbc1410314eb9665321a60f7aeb774727be846e0eaaac1a7c->leave($__internal_92dd56c20888f1dfbc1410314eb9665321a60f7aeb774727be846e0eaaac1a7c_prof);

    }

    public function getTemplateName()
    {
        return "@WebProfiler/Profiler/open.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  90 => 15,  84 => 12,  78 => 11,  75 => 10,  66 => 9,  53 => 5,  50 => 4,  41 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends '@WebProfiler/Profiler/base.html.twig' %}

{% block head %}
    <style>
        {{ include('@WebProfiler/Profiler/open.css.twig') }}
    </style>
{% endblock %}

{% block body %}
<div class=\"header\">
    <h1>{{ file }} <small>line {{ line }}</small></h1>
    <a class=\"doc\" href=\"https://symfony.com/doc/{{ constant('Symfony\\\\Component\\\\HttpKernel\\\\Kernel::VERSION') }}/reference/configuration/framework.html#ide\" rel=\"help\">Open in your IDE?</a>
</div>
<div class=\"source\">
    {{ filename|file_excerpt(line, -1) }}
</div>
{% endblock %}
", "@WebProfiler/Profiler/open.html.twig", "C:\\xampp\\htdocs\\BancoErrores\\vendor\\symfony\\symfony\\src\\Symfony\\Bundle\\WebProfilerBundle\\Resources\\views\\Profiler\\open.html.twig");
    }
}
