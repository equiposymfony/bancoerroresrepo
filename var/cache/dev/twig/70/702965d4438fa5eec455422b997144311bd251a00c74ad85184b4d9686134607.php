<?php

/* users/show.html.twig */
class __TwigTemplate_466b9a83ace7dd0a18276923de7a18c8a76846ed04b5497101bb539fc196350b extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("base.html.twig", "users/show.html.twig", 1);
        $this->blocks = array(
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_d8a8a6d5335979e3383d2c44989a0390d5387a0187363e7fe321d3a2a960182c = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_d8a8a6d5335979e3383d2c44989a0390d5387a0187363e7fe321d3a2a960182c->enter($__internal_d8a8a6d5335979e3383d2c44989a0390d5387a0187363e7fe321d3a2a960182c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "users/show.html.twig"));

        $__internal_e9d4e02260ddaeae293ea416d6be5ba0996c6a4cfd24a71bae676dd25d19689d = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_e9d4e02260ddaeae293ea416d6be5ba0996c6a4cfd24a71bae676dd25d19689d->enter($__internal_e9d4e02260ddaeae293ea416d6be5ba0996c6a4cfd24a71bae676dd25d19689d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "users/show.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_d8a8a6d5335979e3383d2c44989a0390d5387a0187363e7fe321d3a2a960182c->leave($__internal_d8a8a6d5335979e3383d2c44989a0390d5387a0187363e7fe321d3a2a960182c_prof);

        
        $__internal_e9d4e02260ddaeae293ea416d6be5ba0996c6a4cfd24a71bae676dd25d19689d->leave($__internal_e9d4e02260ddaeae293ea416d6be5ba0996c6a4cfd24a71bae676dd25d19689d_prof);

    }

    // line 3
    public function block_body($context, array $blocks = array())
    {
        $__internal_69e3e571f7b46e63f16d0ee9a5d468ef809c3af26a5e6a8e1157bbd84c00e7ba = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_69e3e571f7b46e63f16d0ee9a5d468ef809c3af26a5e6a8e1157bbd84c00e7ba->enter($__internal_69e3e571f7b46e63f16d0ee9a5d468ef809c3af26a5e6a8e1157bbd84c00e7ba_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_f4caa0e9992115699e8066590adad2e6071d278bd9f32eaed8620c6891eebe2e = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_f4caa0e9992115699e8066590adad2e6071d278bd9f32eaed8620c6891eebe2e->enter($__internal_f4caa0e9992115699e8066590adad2e6071d278bd9f32eaed8620c6891eebe2e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 4
        echo "    <h1>User</h1>

    <table>
        <tbody>
            <tr>
                <th>Id</th>
                <td>";
        // line 10
        echo twig_escape_filter($this->env, $this->getAttribute(($context["user"] ?? $this->getContext($context, "user")), "id", array()), "html", null, true);
        echo "</td>
            </tr>
            <tr>
                <th>Iduser</th>
                <td>";
        // line 14
        echo twig_escape_filter($this->env, $this->getAttribute(($context["user"] ?? $this->getContext($context, "user")), "idUser", array()), "html", null, true);
        echo "</td>
            </tr>
            <tr>
                <th>Nameuser</th>
                <td>";
        // line 18
        echo twig_escape_filter($this->env, $this->getAttribute(($context["user"] ?? $this->getContext($context, "user")), "nameUser", array()), "html", null, true);
        echo "</td>
            </tr>
        </tbody>
    </table>

    <ul>
        <li>
            <a href=\"";
        // line 25
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("users_index");
        echo "\">Back to the list</a>
        </li>
        <li>
            <a href=\"";
        // line 28
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("users_edit", array("id" => $this->getAttribute(($context["user"] ?? $this->getContext($context, "user")), "id", array()))), "html", null, true);
        echo "\">Edit</a>
        </li>
        <li>
            ";
        // line 31
        echo         $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->renderBlock(($context["delete_form"] ?? $this->getContext($context, "delete_form")), 'form_start');
        echo "
                <input type=\"submit\" value=\"Delete\">
            ";
        // line 33
        echo         $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->renderBlock(($context["delete_form"] ?? $this->getContext($context, "delete_form")), 'form_end');
        echo "
        </li>
    </ul>
";
        
        $__internal_f4caa0e9992115699e8066590adad2e6071d278bd9f32eaed8620c6891eebe2e->leave($__internal_f4caa0e9992115699e8066590adad2e6071d278bd9f32eaed8620c6891eebe2e_prof);

        
        $__internal_69e3e571f7b46e63f16d0ee9a5d468ef809c3af26a5e6a8e1157bbd84c00e7ba->leave($__internal_69e3e571f7b46e63f16d0ee9a5d468ef809c3af26a5e6a8e1157bbd84c00e7ba_prof);

    }

    public function getTemplateName()
    {
        return "users/show.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  98 => 33,  93 => 31,  87 => 28,  81 => 25,  71 => 18,  64 => 14,  57 => 10,  49 => 4,  40 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends 'base.html.twig' %}

{% block body %}
    <h1>User</h1>

    <table>
        <tbody>
            <tr>
                <th>Id</th>
                <td>{{ user.id }}</td>
            </tr>
            <tr>
                <th>Iduser</th>
                <td>{{ user.idUser }}</td>
            </tr>
            <tr>
                <th>Nameuser</th>
                <td>{{ user.nameUser }}</td>
            </tr>
        </tbody>
    </table>

    <ul>
        <li>
            <a href=\"{{ path('users_index') }}\">Back to the list</a>
        </li>
        <li>
            <a href=\"{{ path('users_edit', { 'id': user.id }) }}\">Edit</a>
        </li>
        <li>
            {{ form_start(delete_form) }}
                <input type=\"submit\" value=\"Delete\">
            {{ form_end(delete_form) }}
        </li>
    </ul>
{% endblock %}
", "users/show.html.twig", "C:\\xampp\\htdocs\\BancoErrores\\app\\Resources\\views\\users\\show.html.twig");
    }
}
