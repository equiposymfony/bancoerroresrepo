<?php

/* @Twig/Exception/exception_full.html.twig */
class __TwigTemplate_f105047b354efc8b735bf76a9266c0d36af85fb6d992e9fbd51ae88d75a78747 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@Twig/layout.html.twig", "@Twig/Exception/exception_full.html.twig", 1);
        $this->blocks = array(
            'head' => array($this, 'block_head'),
            'title' => array($this, 'block_title'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@Twig/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_8ae320a2d48508df4227e9dad63372c0a776fedd584ebb521531563215c63c95 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_8ae320a2d48508df4227e9dad63372c0a776fedd584ebb521531563215c63c95->enter($__internal_8ae320a2d48508df4227e9dad63372c0a776fedd584ebb521531563215c63c95_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Twig/Exception/exception_full.html.twig"));

        $__internal_1459342d642ecf872cd8ee14830f253acd402dc363969f586dce5fa61b9c1233 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_1459342d642ecf872cd8ee14830f253acd402dc363969f586dce5fa61b9c1233->enter($__internal_1459342d642ecf872cd8ee14830f253acd402dc363969f586dce5fa61b9c1233_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Twig/Exception/exception_full.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_8ae320a2d48508df4227e9dad63372c0a776fedd584ebb521531563215c63c95->leave($__internal_8ae320a2d48508df4227e9dad63372c0a776fedd584ebb521531563215c63c95_prof);

        
        $__internal_1459342d642ecf872cd8ee14830f253acd402dc363969f586dce5fa61b9c1233->leave($__internal_1459342d642ecf872cd8ee14830f253acd402dc363969f586dce5fa61b9c1233_prof);

    }

    // line 3
    public function block_head($context, array $blocks = array())
    {
        $__internal_88f427b8183fb3176291e48a02fe4acec5b26371201fe36b3a9b44d2516613e8 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_88f427b8183fb3176291e48a02fe4acec5b26371201fe36b3a9b44d2516613e8->enter($__internal_88f427b8183fb3176291e48a02fe4acec5b26371201fe36b3a9b44d2516613e8_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "head"));

        $__internal_2a9e84bfa0dd7c6a6ede4f6fd9b27d2357236f44b49dbda8c13b89719fffabfc = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_2a9e84bfa0dd7c6a6ede4f6fd9b27d2357236f44b49dbda8c13b89719fffabfc->enter($__internal_2a9e84bfa0dd7c6a6ede4f6fd9b27d2357236f44b49dbda8c13b89719fffabfc_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "head"));

        // line 4
        echo "    <style>
        .sf-reset .traces {
            padding-bottom: 14px;
        }
        .sf-reset .traces li {
            font-size: 12px;
            color: #868686;
            padding: 5px 4px;
            list-style-type: decimal;
            margin-left: 20px;
        }
        .sf-reset #logs .traces li.error {
            font-style: normal;
            color: #AA3333;
            background: #f9ecec;
        }
        .sf-reset #logs .traces li.warning {
            font-style: normal;
            background: #ffcc00;
        }
        /* fix for Opera not liking empty <li> */
        .sf-reset .traces li:after {
            content: \"\\00A0\";
        }
        .sf-reset .trace {
            border: 1px solid #D3D3D3;
            padding: 10px;
            overflow: auto;
            margin: 10px 0 20px;
        }
        .sf-reset .block-exception {
            -moz-border-radius: 16px;
            -webkit-border-radius: 16px;
            border-radius: 16px;
            margin-bottom: 20px;
            background-color: #f6f6f6;
            border: 1px solid #dfdfdf;
            padding: 30px 28px;
            word-wrap: break-word;
            overflow: hidden;
        }
        .sf-reset .block-exception div {
            color: #313131;
            font-size: 10px;
        }
        .sf-reset .block-exception-detected .illustration-exception,
        .sf-reset .block-exception-detected .text-exception {
            float: left;
        }
        .sf-reset .block-exception-detected .illustration-exception {
            width: 152px;
        }
        .sf-reset .block-exception-detected .text-exception {
            width: 670px;
            padding: 30px 44px 24px 46px;
            position: relative;
        }
        .sf-reset .text-exception .open-quote,
        .sf-reset .text-exception .close-quote {
            font-family: Arial, Helvetica, sans-serif;
            position: absolute;
            color: #C9C9C9;
            font-size: 8em;
        }
        .sf-reset .open-quote {
            top: 0;
            left: 0;
        }
        .sf-reset .close-quote {
            bottom: -0.5em;
            right: 50px;
        }
        .sf-reset .block-exception p {
            font-family: Arial, Helvetica, sans-serif;
        }
        .sf-reset .block-exception p a,
        .sf-reset .block-exception p a:hover {
            color: #565656;
        }
        .sf-reset .logs h2 {
            float: left;
            width: 654px;
        }
        .sf-reset .error-count, .sf-reset .support {
            float: right;
            width: 170px;
            text-align: right;
        }
        .sf-reset .error-count span {
             display: inline-block;
             background-color: #aacd4e;
             -moz-border-radius: 6px;
             -webkit-border-radius: 6px;
             border-radius: 6px;
             padding: 4px;
             color: white;
             margin-right: 2px;
             font-size: 11px;
             font-weight: bold;
        }

        .sf-reset .support a {
            display: inline-block;
            -moz-border-radius: 6px;
            -webkit-border-radius: 6px;
            border-radius: 6px;
            padding: 4px;
            color: #000000;
            margin-right: 2px;
            font-size: 11px;
            font-weight: bold;
        }

        .sf-reset .toggle {
            vertical-align: middle;
        }
        .sf-reset .linked ul,
        .sf-reset .linked li {
            display: inline;
        }
        .sf-reset #output-content {
            color: #000;
            font-size: 12px;
        }
        .sf-reset #traces-text pre {
            white-space: pre;
            font-size: 12px;
            font-family: monospace;
        }
    </style>
";
        
        $__internal_2a9e84bfa0dd7c6a6ede4f6fd9b27d2357236f44b49dbda8c13b89719fffabfc->leave($__internal_2a9e84bfa0dd7c6a6ede4f6fd9b27d2357236f44b49dbda8c13b89719fffabfc_prof);

        
        $__internal_88f427b8183fb3176291e48a02fe4acec5b26371201fe36b3a9b44d2516613e8->leave($__internal_88f427b8183fb3176291e48a02fe4acec5b26371201fe36b3a9b44d2516613e8_prof);

    }

    // line 136
    public function block_title($context, array $blocks = array())
    {
        $__internal_366e4700842943823af34646374b02b5394d15a69d237a1384a25e65272980cb = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_366e4700842943823af34646374b02b5394d15a69d237a1384a25e65272980cb->enter($__internal_366e4700842943823af34646374b02b5394d15a69d237a1384a25e65272980cb_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        $__internal_d947fd74c08bc369627ca9964d9e5cfb27788c97e2c9173c24bd973d582729f9 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_d947fd74c08bc369627ca9964d9e5cfb27788c97e2c9173c24bd973d582729f9->enter($__internal_d947fd74c08bc369627ca9964d9e5cfb27788c97e2c9173c24bd973d582729f9_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        // line 137
        echo "    ";
        echo twig_escape_filter($this->env, $this->getAttribute(($context["exception"] ?? $this->getContext($context, "exception")), "message", array()), "html", null, true);
        echo " (";
        echo twig_escape_filter($this->env, ($context["status_code"] ?? $this->getContext($context, "status_code")), "html", null, true);
        echo " ";
        echo twig_escape_filter($this->env, ($context["status_text"] ?? $this->getContext($context, "status_text")), "html", null, true);
        echo ")
";
        
        $__internal_d947fd74c08bc369627ca9964d9e5cfb27788c97e2c9173c24bd973d582729f9->leave($__internal_d947fd74c08bc369627ca9964d9e5cfb27788c97e2c9173c24bd973d582729f9_prof);

        
        $__internal_366e4700842943823af34646374b02b5394d15a69d237a1384a25e65272980cb->leave($__internal_366e4700842943823af34646374b02b5394d15a69d237a1384a25e65272980cb_prof);

    }

    // line 140
    public function block_body($context, array $blocks = array())
    {
        $__internal_ea752d0f4d84f40bfd2f2e215d2d72b6a7c142330dfbc73f0f853cc701ee9cea = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_ea752d0f4d84f40bfd2f2e215d2d72b6a7c142330dfbc73f0f853cc701ee9cea->enter($__internal_ea752d0f4d84f40bfd2f2e215d2d72b6a7c142330dfbc73f0f853cc701ee9cea_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_581c9f3bb159ed2653f6a8dcc60aba69e617e029e684b05425668c0d9b436863 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_581c9f3bb159ed2653f6a8dcc60aba69e617e029e684b05425668c0d9b436863->enter($__internal_581c9f3bb159ed2653f6a8dcc60aba69e617e029e684b05425668c0d9b436863_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 141
        echo "    ";
        $this->loadTemplate("@Twig/Exception/exception.html.twig", "@Twig/Exception/exception_full.html.twig", 141)->display($context);
        
        $__internal_581c9f3bb159ed2653f6a8dcc60aba69e617e029e684b05425668c0d9b436863->leave($__internal_581c9f3bb159ed2653f6a8dcc60aba69e617e029e684b05425668c0d9b436863_prof);

        
        $__internal_ea752d0f4d84f40bfd2f2e215d2d72b6a7c142330dfbc73f0f853cc701ee9cea->leave($__internal_ea752d0f4d84f40bfd2f2e215d2d72b6a7c142330dfbc73f0f853cc701ee9cea_prof);

    }

    public function getTemplateName()
    {
        return "@Twig/Exception/exception_full.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  226 => 141,  217 => 140,  200 => 137,  191 => 136,  51 => 4,  42 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends '@Twig/layout.html.twig' %}

{% block head %}
    <style>
        .sf-reset .traces {
            padding-bottom: 14px;
        }
        .sf-reset .traces li {
            font-size: 12px;
            color: #868686;
            padding: 5px 4px;
            list-style-type: decimal;
            margin-left: 20px;
        }
        .sf-reset #logs .traces li.error {
            font-style: normal;
            color: #AA3333;
            background: #f9ecec;
        }
        .sf-reset #logs .traces li.warning {
            font-style: normal;
            background: #ffcc00;
        }
        /* fix for Opera not liking empty <li> */
        .sf-reset .traces li:after {
            content: \"\\00A0\";
        }
        .sf-reset .trace {
            border: 1px solid #D3D3D3;
            padding: 10px;
            overflow: auto;
            margin: 10px 0 20px;
        }
        .sf-reset .block-exception {
            -moz-border-radius: 16px;
            -webkit-border-radius: 16px;
            border-radius: 16px;
            margin-bottom: 20px;
            background-color: #f6f6f6;
            border: 1px solid #dfdfdf;
            padding: 30px 28px;
            word-wrap: break-word;
            overflow: hidden;
        }
        .sf-reset .block-exception div {
            color: #313131;
            font-size: 10px;
        }
        .sf-reset .block-exception-detected .illustration-exception,
        .sf-reset .block-exception-detected .text-exception {
            float: left;
        }
        .sf-reset .block-exception-detected .illustration-exception {
            width: 152px;
        }
        .sf-reset .block-exception-detected .text-exception {
            width: 670px;
            padding: 30px 44px 24px 46px;
            position: relative;
        }
        .sf-reset .text-exception .open-quote,
        .sf-reset .text-exception .close-quote {
            font-family: Arial, Helvetica, sans-serif;
            position: absolute;
            color: #C9C9C9;
            font-size: 8em;
        }
        .sf-reset .open-quote {
            top: 0;
            left: 0;
        }
        .sf-reset .close-quote {
            bottom: -0.5em;
            right: 50px;
        }
        .sf-reset .block-exception p {
            font-family: Arial, Helvetica, sans-serif;
        }
        .sf-reset .block-exception p a,
        .sf-reset .block-exception p a:hover {
            color: #565656;
        }
        .sf-reset .logs h2 {
            float: left;
            width: 654px;
        }
        .sf-reset .error-count, .sf-reset .support {
            float: right;
            width: 170px;
            text-align: right;
        }
        .sf-reset .error-count span {
             display: inline-block;
             background-color: #aacd4e;
             -moz-border-radius: 6px;
             -webkit-border-radius: 6px;
             border-radius: 6px;
             padding: 4px;
             color: white;
             margin-right: 2px;
             font-size: 11px;
             font-weight: bold;
        }

        .sf-reset .support a {
            display: inline-block;
            -moz-border-radius: 6px;
            -webkit-border-radius: 6px;
            border-radius: 6px;
            padding: 4px;
            color: #000000;
            margin-right: 2px;
            font-size: 11px;
            font-weight: bold;
        }

        .sf-reset .toggle {
            vertical-align: middle;
        }
        .sf-reset .linked ul,
        .sf-reset .linked li {
            display: inline;
        }
        .sf-reset #output-content {
            color: #000;
            font-size: 12px;
        }
        .sf-reset #traces-text pre {
            white-space: pre;
            font-size: 12px;
            font-family: monospace;
        }
    </style>
{% endblock %}

{% block title %}
    {{ exception.message }} ({{ status_code }} {{ status_text }})
{% endblock %}

{% block body %}
    {% include '@Twig/Exception/exception.html.twig' %}
{% endblock %}
", "@Twig/Exception/exception_full.html.twig", "C:\\xampp\\htdocs\\BancoErrores\\vendor\\symfony\\symfony\\src\\Symfony\\Bundle\\TwigBundle\\Resources\\views\\Exception\\exception_full.html.twig");
    }
}
