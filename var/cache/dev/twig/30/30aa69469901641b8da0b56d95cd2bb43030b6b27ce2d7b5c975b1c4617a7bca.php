<?php

/* default/login.html.twig */
class __TwigTemplate_fe2c1d15a4cd0b211afe3c614b6ecbadfb5abf591e512d985e4f049ccec0a972 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_4efa4d8f7d16ebac54398acdafbbb50f1bd2bc33fa29c66c8fbd6a66ffea33a5 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_4efa4d8f7d16ebac54398acdafbbb50f1bd2bc33fa29c66c8fbd6a66ffea33a5->enter($__internal_4efa4d8f7d16ebac54398acdafbbb50f1bd2bc33fa29c66c8fbd6a66ffea33a5_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "default/login.html.twig"));

        $__internal_becce0d53028ae63b3cd5bb9b08bfd74578fdb6823acfaa3b9638d75890b4daa = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_becce0d53028ae63b3cd5bb9b08bfd74578fdb6823acfaa3b9638d75890b4daa->enter($__internal_becce0d53028ae63b3cd5bb9b08bfd74578fdb6823acfaa3b9638d75890b4daa_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "default/login.html.twig"));

        // line 1
        echo "<form action=\"<?php echo \$view['router']->path('login') ?>\" method=\"post\">
    <label for=\"username\">Username:</label>
    <input type=\"text\" id=\"username\" name=\"_username\" value=\"<?php echo \$last_username ?>\" />

    <label for=\"password\">Password:</label>
    <input type=\"password\" id=\"password\" name=\"_password\" />

    <!--
        If you want to control the URL the user
        is redirected to on success (more details below)
        <input type=\"hidden\" name=\"_target_path\" value=\"/account\" />
    -->

    <button type=\"submit\">login</button>
</form>";
        
        $__internal_4efa4d8f7d16ebac54398acdafbbb50f1bd2bc33fa29c66c8fbd6a66ffea33a5->leave($__internal_4efa4d8f7d16ebac54398acdafbbb50f1bd2bc33fa29c66c8fbd6a66ffea33a5_prof);

        
        $__internal_becce0d53028ae63b3cd5bb9b08bfd74578fdb6823acfaa3b9638d75890b4daa->leave($__internal_becce0d53028ae63b3cd5bb9b08bfd74578fdb6823acfaa3b9638d75890b4daa_prof);

    }

    public function getTemplateName()
    {
        return "default/login.html.twig";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<form action=\"<?php echo \$view['router']->path('login') ?>\" method=\"post\">
    <label for=\"username\">Username:</label>
    <input type=\"text\" id=\"username\" name=\"_username\" value=\"<?php echo \$last_username ?>\" />

    <label for=\"password\">Password:</label>
    <input type=\"password\" id=\"password\" name=\"_password\" />

    <!--
        If you want to control the URL the user
        is redirected to on success (more details below)
        <input type=\"hidden\" name=\"_target_path\" value=\"/account\" />
    -->

    <button type=\"submit\">login</button>
</form>", "default/login.html.twig", "C:\\xampp\\htdocs\\BancoErrores\\app\\Resources\\views\\default\\login.html.twig");
    }
}
