<?php

/* @WebProfiler/Collector/router.html.twig */
class __TwigTemplate_0a49730a4415c52dbae0bde59acb7a1499983a905e405c913fd1a90537bf38c4 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@WebProfiler/Profiler/layout.html.twig", "@WebProfiler/Collector/router.html.twig", 1);
        $this->blocks = array(
            'toolbar' => array($this, 'block_toolbar'),
            'menu' => array($this, 'block_menu'),
            'panel' => array($this, 'block_panel'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@WebProfiler/Profiler/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_709aa446c5d59a9908b3ad488a2aa93f264969c090dad101df5ce60878153785 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_709aa446c5d59a9908b3ad488a2aa93f264969c090dad101df5ce60878153785->enter($__internal_709aa446c5d59a9908b3ad488a2aa93f264969c090dad101df5ce60878153785_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@WebProfiler/Collector/router.html.twig"));

        $__internal_a6b88dd9c76d303daed1e346f5b928939241867e9598d02c4cd311a7565c0ce3 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_a6b88dd9c76d303daed1e346f5b928939241867e9598d02c4cd311a7565c0ce3->enter($__internal_a6b88dd9c76d303daed1e346f5b928939241867e9598d02c4cd311a7565c0ce3_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@WebProfiler/Collector/router.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_709aa446c5d59a9908b3ad488a2aa93f264969c090dad101df5ce60878153785->leave($__internal_709aa446c5d59a9908b3ad488a2aa93f264969c090dad101df5ce60878153785_prof);

        
        $__internal_a6b88dd9c76d303daed1e346f5b928939241867e9598d02c4cd311a7565c0ce3->leave($__internal_a6b88dd9c76d303daed1e346f5b928939241867e9598d02c4cd311a7565c0ce3_prof);

    }

    // line 3
    public function block_toolbar($context, array $blocks = array())
    {
        $__internal_cb81a017da25231fd149f7931927285b20438131c56d2e55f42f495364222ecb = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_cb81a017da25231fd149f7931927285b20438131c56d2e55f42f495364222ecb->enter($__internal_cb81a017da25231fd149f7931927285b20438131c56d2e55f42f495364222ecb_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "toolbar"));

        $__internal_e06df6f239990b8659d80826068a1da9e8541145d67309755d4e648827df31f4 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_e06df6f239990b8659d80826068a1da9e8541145d67309755d4e648827df31f4->enter($__internal_e06df6f239990b8659d80826068a1da9e8541145d67309755d4e648827df31f4_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "toolbar"));

        
        $__internal_e06df6f239990b8659d80826068a1da9e8541145d67309755d4e648827df31f4->leave($__internal_e06df6f239990b8659d80826068a1da9e8541145d67309755d4e648827df31f4_prof);

        
        $__internal_cb81a017da25231fd149f7931927285b20438131c56d2e55f42f495364222ecb->leave($__internal_cb81a017da25231fd149f7931927285b20438131c56d2e55f42f495364222ecb_prof);

    }

    // line 5
    public function block_menu($context, array $blocks = array())
    {
        $__internal_39b913cc809fd7ee1f13b7f8c3ed83d5e0b065cfa536cd9243da128c661ba064 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_39b913cc809fd7ee1f13b7f8c3ed83d5e0b065cfa536cd9243da128c661ba064->enter($__internal_39b913cc809fd7ee1f13b7f8c3ed83d5e0b065cfa536cd9243da128c661ba064_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "menu"));

        $__internal_ab6aec6b32fea0dfe98921414be839dc64e8afe789390e3d986d1372d3151364 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_ab6aec6b32fea0dfe98921414be839dc64e8afe789390e3d986d1372d3151364->enter($__internal_ab6aec6b32fea0dfe98921414be839dc64e8afe789390e3d986d1372d3151364_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "menu"));

        // line 6
        echo "<span class=\"label\">
    <span class=\"icon\">";
        // line 7
        echo twig_include($this->env, $context, "@WebProfiler/Icon/router.svg");
        echo "</span>
    <strong>Routing</strong>
</span>
";
        
        $__internal_ab6aec6b32fea0dfe98921414be839dc64e8afe789390e3d986d1372d3151364->leave($__internal_ab6aec6b32fea0dfe98921414be839dc64e8afe789390e3d986d1372d3151364_prof);

        
        $__internal_39b913cc809fd7ee1f13b7f8c3ed83d5e0b065cfa536cd9243da128c661ba064->leave($__internal_39b913cc809fd7ee1f13b7f8c3ed83d5e0b065cfa536cd9243da128c661ba064_prof);

    }

    // line 12
    public function block_panel($context, array $blocks = array())
    {
        $__internal_c2c022825417938dcab9cd5a423341062e2029019107a831e2b5200e78885a10 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_c2c022825417938dcab9cd5a423341062e2029019107a831e2b5200e78885a10->enter($__internal_c2c022825417938dcab9cd5a423341062e2029019107a831e2b5200e78885a10_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "panel"));

        $__internal_37e00d7d85273a4f37899d47927da63ebe5fd1b193c0a9845781f6038b45ce23 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_37e00d7d85273a4f37899d47927da63ebe5fd1b193c0a9845781f6038b45ce23->enter($__internal_37e00d7d85273a4f37899d47927da63ebe5fd1b193c0a9845781f6038b45ce23_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "panel"));

        // line 13
        echo "    ";
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Extension\HttpKernelRuntime')->renderFragment($this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_profiler_router", array("token" => ($context["token"] ?? $this->getContext($context, "token")))));
        echo "
";
        
        $__internal_37e00d7d85273a4f37899d47927da63ebe5fd1b193c0a9845781f6038b45ce23->leave($__internal_37e00d7d85273a4f37899d47927da63ebe5fd1b193c0a9845781f6038b45ce23_prof);

        
        $__internal_c2c022825417938dcab9cd5a423341062e2029019107a831e2b5200e78885a10->leave($__internal_c2c022825417938dcab9cd5a423341062e2029019107a831e2b5200e78885a10_prof);

    }

    public function getTemplateName()
    {
        return "@WebProfiler/Collector/router.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  94 => 13,  85 => 12,  71 => 7,  68 => 6,  59 => 5,  42 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends '@WebProfiler/Profiler/layout.html.twig' %}

{% block toolbar %}{% endblock %}

{% block menu %}
<span class=\"label\">
    <span class=\"icon\">{{ include('@WebProfiler/Icon/router.svg') }}</span>
    <strong>Routing</strong>
</span>
{% endblock %}

{% block panel %}
    {{ render(path('_profiler_router', { token: token })) }}
{% endblock %}
", "@WebProfiler/Collector/router.html.twig", "C:\\xampp\\htdocs\\BancoErrores\\vendor\\symfony\\symfony\\src\\Symfony\\Bundle\\WebProfilerBundle\\Resources\\views\\Collector\\router.html.twig");
    }
}
