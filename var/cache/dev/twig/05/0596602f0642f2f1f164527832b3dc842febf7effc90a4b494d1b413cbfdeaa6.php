<?php

/* @WebProfiler/Collector/exception.html.twig */
class __TwigTemplate_e12506caa6aa0b489a5a6e3aed6a7d8b525d78ffcaed02e7eb56bb15cc405767 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@WebProfiler/Profiler/layout.html.twig", "@WebProfiler/Collector/exception.html.twig", 1);
        $this->blocks = array(
            'head' => array($this, 'block_head'),
            'menu' => array($this, 'block_menu'),
            'panel' => array($this, 'block_panel'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@WebProfiler/Profiler/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_337edf646b916ee472236c3b6116df0a661032ea7e6081d2ee2125723aab866c = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_337edf646b916ee472236c3b6116df0a661032ea7e6081d2ee2125723aab866c->enter($__internal_337edf646b916ee472236c3b6116df0a661032ea7e6081d2ee2125723aab866c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@WebProfiler/Collector/exception.html.twig"));

        $__internal_cd8d4b1a7b9bbad6e37fc9ef7ee29460aedf98bf8f60277d96040caadfef3396 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_cd8d4b1a7b9bbad6e37fc9ef7ee29460aedf98bf8f60277d96040caadfef3396->enter($__internal_cd8d4b1a7b9bbad6e37fc9ef7ee29460aedf98bf8f60277d96040caadfef3396_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@WebProfiler/Collector/exception.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_337edf646b916ee472236c3b6116df0a661032ea7e6081d2ee2125723aab866c->leave($__internal_337edf646b916ee472236c3b6116df0a661032ea7e6081d2ee2125723aab866c_prof);

        
        $__internal_cd8d4b1a7b9bbad6e37fc9ef7ee29460aedf98bf8f60277d96040caadfef3396->leave($__internal_cd8d4b1a7b9bbad6e37fc9ef7ee29460aedf98bf8f60277d96040caadfef3396_prof);

    }

    // line 3
    public function block_head($context, array $blocks = array())
    {
        $__internal_99aa1e12363cc1fb9ddf56a9cbd3d2da98cc24a011e3979351498e3f6c41c712 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_99aa1e12363cc1fb9ddf56a9cbd3d2da98cc24a011e3979351498e3f6c41c712->enter($__internal_99aa1e12363cc1fb9ddf56a9cbd3d2da98cc24a011e3979351498e3f6c41c712_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "head"));

        $__internal_2e6fb42bd5969030ee2a12a212729850339bb7f6739fb1fb3a817b792b64edc6 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_2e6fb42bd5969030ee2a12a212729850339bb7f6739fb1fb3a817b792b64edc6->enter($__internal_2e6fb42bd5969030ee2a12a212729850339bb7f6739fb1fb3a817b792b64edc6_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "head"));

        // line 4
        echo "    ";
        if ($this->getAttribute(($context["collector"] ?? $this->getContext($context, "collector")), "hasexception", array())) {
            // line 5
            echo "        <style>
            ";
            // line 6
            echo $this->env->getRuntime('Symfony\Bridge\Twig\Extension\HttpKernelRuntime')->renderFragment($this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_profiler_exception_css", array("token" => ($context["token"] ?? $this->getContext($context, "token")))));
            echo "
        </style>
    ";
        }
        // line 9
        echo "    ";
        $this->displayParentBlock("head", $context, $blocks);
        echo "
";
        
        $__internal_2e6fb42bd5969030ee2a12a212729850339bb7f6739fb1fb3a817b792b64edc6->leave($__internal_2e6fb42bd5969030ee2a12a212729850339bb7f6739fb1fb3a817b792b64edc6_prof);

        
        $__internal_99aa1e12363cc1fb9ddf56a9cbd3d2da98cc24a011e3979351498e3f6c41c712->leave($__internal_99aa1e12363cc1fb9ddf56a9cbd3d2da98cc24a011e3979351498e3f6c41c712_prof);

    }

    // line 12
    public function block_menu($context, array $blocks = array())
    {
        $__internal_7f567d761df86475604881309f5f4c0051d87ed5fd380469daf1b23238eddef0 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_7f567d761df86475604881309f5f4c0051d87ed5fd380469daf1b23238eddef0->enter($__internal_7f567d761df86475604881309f5f4c0051d87ed5fd380469daf1b23238eddef0_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "menu"));

        $__internal_f8d525ebafc379ba129bd432821917ec6c3fc0a9a85e0586db082190fa4ad85b = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_f8d525ebafc379ba129bd432821917ec6c3fc0a9a85e0586db082190fa4ad85b->enter($__internal_f8d525ebafc379ba129bd432821917ec6c3fc0a9a85e0586db082190fa4ad85b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "menu"));

        // line 13
        echo "    <span class=\"label ";
        echo (($this->getAttribute(($context["collector"] ?? $this->getContext($context, "collector")), "hasexception", array())) ? ("label-status-error") : ("disabled"));
        echo "\">
        <span class=\"icon\">";
        // line 14
        echo twig_include($this->env, $context, "@WebProfiler/Icon/exception.svg");
        echo "</span>
        <strong>Exception</strong>
        ";
        // line 16
        if ($this->getAttribute(($context["collector"] ?? $this->getContext($context, "collector")), "hasexception", array())) {
            // line 17
            echo "            <span class=\"count\">
                <span>1</span>
            </span>
        ";
        }
        // line 21
        echo "    </span>
";
        
        $__internal_f8d525ebafc379ba129bd432821917ec6c3fc0a9a85e0586db082190fa4ad85b->leave($__internal_f8d525ebafc379ba129bd432821917ec6c3fc0a9a85e0586db082190fa4ad85b_prof);

        
        $__internal_7f567d761df86475604881309f5f4c0051d87ed5fd380469daf1b23238eddef0->leave($__internal_7f567d761df86475604881309f5f4c0051d87ed5fd380469daf1b23238eddef0_prof);

    }

    // line 24
    public function block_panel($context, array $blocks = array())
    {
        $__internal_5713f69c760d282eb6805b2395bd0cb8ecd6e75577ddc5b71bbf4c5c54961ce0 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_5713f69c760d282eb6805b2395bd0cb8ecd6e75577ddc5b71bbf4c5c54961ce0->enter($__internal_5713f69c760d282eb6805b2395bd0cb8ecd6e75577ddc5b71bbf4c5c54961ce0_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "panel"));

        $__internal_456a61e77b784eb9270015fe286931060fc25653273af2abbd7c96e06df25002 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_456a61e77b784eb9270015fe286931060fc25653273af2abbd7c96e06df25002->enter($__internal_456a61e77b784eb9270015fe286931060fc25653273af2abbd7c96e06df25002_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "panel"));

        // line 25
        echo "    <h2>Exceptions</h2>

    ";
        // line 27
        if ( !$this->getAttribute(($context["collector"] ?? $this->getContext($context, "collector")), "hasexception", array())) {
            // line 28
            echo "        <div class=\"empty\">
            <p>No exception was thrown and caught during the request.</p>
        </div>
    ";
        } else {
            // line 32
            echo "        <div class=\"sf-reset\">
            ";
            // line 33
            echo $this->env->getRuntime('Symfony\Bridge\Twig\Extension\HttpKernelRuntime')->renderFragment($this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_profiler_exception", array("token" => ($context["token"] ?? $this->getContext($context, "token")))));
            echo "
        </div>
    ";
        }
        
        $__internal_456a61e77b784eb9270015fe286931060fc25653273af2abbd7c96e06df25002->leave($__internal_456a61e77b784eb9270015fe286931060fc25653273af2abbd7c96e06df25002_prof);

        
        $__internal_5713f69c760d282eb6805b2395bd0cb8ecd6e75577ddc5b71bbf4c5c54961ce0->leave($__internal_5713f69c760d282eb6805b2395bd0cb8ecd6e75577ddc5b71bbf4c5c54961ce0_prof);

    }

    public function getTemplateName()
    {
        return "@WebProfiler/Collector/exception.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  138 => 33,  135 => 32,  129 => 28,  127 => 27,  123 => 25,  114 => 24,  103 => 21,  97 => 17,  95 => 16,  90 => 14,  85 => 13,  76 => 12,  63 => 9,  57 => 6,  54 => 5,  51 => 4,  42 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends '@WebProfiler/Profiler/layout.html.twig' %}

{% block head %}
    {% if collector.hasexception %}
        <style>
            {{ render(path('_profiler_exception_css', { token: token })) }}
        </style>
    {% endif %}
    {{ parent() }}
{% endblock %}

{% block menu %}
    <span class=\"label {{ collector.hasexception ? 'label-status-error' : 'disabled' }}\">
        <span class=\"icon\">{{ include('@WebProfiler/Icon/exception.svg') }}</span>
        <strong>Exception</strong>
        {% if collector.hasexception %}
            <span class=\"count\">
                <span>1</span>
            </span>
        {% endif %}
    </span>
{% endblock %}

{% block panel %}
    <h2>Exceptions</h2>

    {% if not collector.hasexception %}
        <div class=\"empty\">
            <p>No exception was thrown and caught during the request.</p>
        </div>
    {% else %}
        <div class=\"sf-reset\">
            {{ render(path('_profiler_exception', { token: token })) }}
        </div>
    {% endif %}
{% endblock %}
", "@WebProfiler/Collector/exception.html.twig", "C:\\xampp\\htdocs\\BancoErrores\\vendor\\symfony\\symfony\\src\\Symfony\\Bundle\\WebProfilerBundle\\Resources\\views\\Collector\\exception.html.twig");
    }
}
