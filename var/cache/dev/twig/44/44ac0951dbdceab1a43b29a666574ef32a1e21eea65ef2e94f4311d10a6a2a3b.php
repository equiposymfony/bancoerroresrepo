<?php

/* users/edit.html.twig */
class __TwigTemplate_5b36f032cac2b74e4c841e00af25f91cd2056840b7d1f11a589e9d4af16bd343 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("base.html.twig", "users/edit.html.twig", 1);
        $this->blocks = array(
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_fa6ccd61374e0f7fba07ba21fa1bb4ac0bd2a248d06db839fac079de379279be = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_fa6ccd61374e0f7fba07ba21fa1bb4ac0bd2a248d06db839fac079de379279be->enter($__internal_fa6ccd61374e0f7fba07ba21fa1bb4ac0bd2a248d06db839fac079de379279be_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "users/edit.html.twig"));

        $__internal_d169fa67a2435966590be7683b3091b1df17f7955aeb96b1174c73b3357a02bb = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_d169fa67a2435966590be7683b3091b1df17f7955aeb96b1174c73b3357a02bb->enter($__internal_d169fa67a2435966590be7683b3091b1df17f7955aeb96b1174c73b3357a02bb_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "users/edit.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_fa6ccd61374e0f7fba07ba21fa1bb4ac0bd2a248d06db839fac079de379279be->leave($__internal_fa6ccd61374e0f7fba07ba21fa1bb4ac0bd2a248d06db839fac079de379279be_prof);

        
        $__internal_d169fa67a2435966590be7683b3091b1df17f7955aeb96b1174c73b3357a02bb->leave($__internal_d169fa67a2435966590be7683b3091b1df17f7955aeb96b1174c73b3357a02bb_prof);

    }

    // line 3
    public function block_body($context, array $blocks = array())
    {
        $__internal_d05235b70f7b52e691497618945f649bb9075b49f02c2751419d3e7cfe82a5e0 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_d05235b70f7b52e691497618945f649bb9075b49f02c2751419d3e7cfe82a5e0->enter($__internal_d05235b70f7b52e691497618945f649bb9075b49f02c2751419d3e7cfe82a5e0_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_e4cc5ec8ba934c854595b0f747fca51ef365b00e9f221d7e0f20ae0534961b91 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_e4cc5ec8ba934c854595b0f747fca51ef365b00e9f221d7e0f20ae0534961b91->enter($__internal_e4cc5ec8ba934c854595b0f747fca51ef365b00e9f221d7e0f20ae0534961b91_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 4
        echo "    <h1>User edit</h1>

    ";
        // line 6
        echo         $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->renderBlock(($context["edit_form"] ?? $this->getContext($context, "edit_form")), 'form_start');
        echo "
        ";
        // line 7
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock(($context["edit_form"] ?? $this->getContext($context, "edit_form")), 'widget');
        echo "
        <input type=\"submit\" value=\"Edit\" />
    ";
        // line 9
        echo         $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->renderBlock(($context["edit_form"] ?? $this->getContext($context, "edit_form")), 'form_end');
        echo "

    <ul>
        <li>
            <a href=\"";
        // line 13
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("users_index");
        echo "\">Back to the list</a>
        </li>
        <li>
            ";
        // line 16
        echo         $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->renderBlock(($context["delete_form"] ?? $this->getContext($context, "delete_form")), 'form_start');
        echo "
                <input type=\"submit\" value=\"Delete\">
            ";
        // line 18
        echo         $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->renderBlock(($context["delete_form"] ?? $this->getContext($context, "delete_form")), 'form_end');
        echo "
        </li>
    </ul>
";
        
        $__internal_e4cc5ec8ba934c854595b0f747fca51ef365b00e9f221d7e0f20ae0534961b91->leave($__internal_e4cc5ec8ba934c854595b0f747fca51ef365b00e9f221d7e0f20ae0534961b91_prof);

        
        $__internal_d05235b70f7b52e691497618945f649bb9075b49f02c2751419d3e7cfe82a5e0->leave($__internal_d05235b70f7b52e691497618945f649bb9075b49f02c2751419d3e7cfe82a5e0_prof);

    }

    public function getTemplateName()
    {
        return "users/edit.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  80 => 18,  75 => 16,  69 => 13,  62 => 9,  57 => 7,  53 => 6,  49 => 4,  40 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends 'base.html.twig' %}

{% block body %}
    <h1>User edit</h1>

    {{ form_start(edit_form) }}
        {{ form_widget(edit_form) }}
        <input type=\"submit\" value=\"Edit\" />
    {{ form_end(edit_form) }}

    <ul>
        <li>
            <a href=\"{{ path('users_index') }}\">Back to the list</a>
        </li>
        <li>
            {{ form_start(delete_form) }}
                <input type=\"submit\" value=\"Delete\">
            {{ form_end(delete_form) }}
        </li>
    </ul>
{% endblock %}
", "users/edit.html.twig", "C:\\xampp\\htdocs\\BancoErrores\\app\\Resources\\views\\users\\edit.html.twig");
    }
}
