<?php

/* users/new.html.twig */
class __TwigTemplate_875cef6326f45d161b21e5f9c7960f0e61142a8dfe50d9d90c8babced419d278 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("base.html.twig", "users/new.html.twig", 1);
        $this->blocks = array(
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_7d470c04f7cac82edfe490f9aa1b3fbf0afb29a66f5e236b42b3db5bc374f05d = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_7d470c04f7cac82edfe490f9aa1b3fbf0afb29a66f5e236b42b3db5bc374f05d->enter($__internal_7d470c04f7cac82edfe490f9aa1b3fbf0afb29a66f5e236b42b3db5bc374f05d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "users/new.html.twig"));

        $__internal_292b1c5092308385c4f304ce90f01045b26787bbc27e8d889b5fe64ed3cf2f8f = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_292b1c5092308385c4f304ce90f01045b26787bbc27e8d889b5fe64ed3cf2f8f->enter($__internal_292b1c5092308385c4f304ce90f01045b26787bbc27e8d889b5fe64ed3cf2f8f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "users/new.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_7d470c04f7cac82edfe490f9aa1b3fbf0afb29a66f5e236b42b3db5bc374f05d->leave($__internal_7d470c04f7cac82edfe490f9aa1b3fbf0afb29a66f5e236b42b3db5bc374f05d_prof);

        
        $__internal_292b1c5092308385c4f304ce90f01045b26787bbc27e8d889b5fe64ed3cf2f8f->leave($__internal_292b1c5092308385c4f304ce90f01045b26787bbc27e8d889b5fe64ed3cf2f8f_prof);

    }

    // line 3
    public function block_body($context, array $blocks = array())
    {
        $__internal_9493365b8c0ae86bfd7c595d51ab10a2dbf4d2117e6b3889d833ce086c3cc221 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_9493365b8c0ae86bfd7c595d51ab10a2dbf4d2117e6b3889d833ce086c3cc221->enter($__internal_9493365b8c0ae86bfd7c595d51ab10a2dbf4d2117e6b3889d833ce086c3cc221_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_627aae5fbda8d4f8bb849cf533185c45ad6cbd2fd9a8c2404b83e7210df87462 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_627aae5fbda8d4f8bb849cf533185c45ad6cbd2fd9a8c2404b83e7210df87462->enter($__internal_627aae5fbda8d4f8bb849cf533185c45ad6cbd2fd9a8c2404b83e7210df87462_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 4
        echo "    <h1>User creation</h1>

    ";
        // line 6
        echo         $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->renderBlock(($context["form"] ?? $this->getContext($context, "form")), 'form_start');
        echo "
        ";
        // line 7
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock(($context["form"] ?? $this->getContext($context, "form")), 'widget');
        echo "
        <input type=\"submit\" value=\"Create\" />
    ";
        // line 9
        echo         $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->renderBlock(($context["form"] ?? $this->getContext($context, "form")), 'form_end');
        echo "

    <ul>
        <li>
            <a href=\"";
        // line 13
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("users_index");
        echo "\">Back to the list</a>
        </li>
    </ul>
";
        
        $__internal_627aae5fbda8d4f8bb849cf533185c45ad6cbd2fd9a8c2404b83e7210df87462->leave($__internal_627aae5fbda8d4f8bb849cf533185c45ad6cbd2fd9a8c2404b83e7210df87462_prof);

        
        $__internal_9493365b8c0ae86bfd7c595d51ab10a2dbf4d2117e6b3889d833ce086c3cc221->leave($__internal_9493365b8c0ae86bfd7c595d51ab10a2dbf4d2117e6b3889d833ce086c3cc221_prof);

    }

    public function getTemplateName()
    {
        return "users/new.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  69 => 13,  62 => 9,  57 => 7,  53 => 6,  49 => 4,  40 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends 'base.html.twig' %}

{% block body %}
    <h1>User creation</h1>

    {{ form_start(form) }}
        {{ form_widget(form) }}
        <input type=\"submit\" value=\"Create\" />
    {{ form_end(form) }}

    <ul>
        <li>
            <a href=\"{{ path('users_index') }}\">Back to the list</a>
        </li>
    </ul>
{% endblock %}
", "users/new.html.twig", "C:\\xampp\\htdocs\\BancoErrores\\app\\Resources\\views\\users\\new.html.twig");
    }
}
