<?php

/* @Twig/images/chevron-right.svg */
class __TwigTemplate_66394f4fdcf47c662c21f334aa6985a793e3261f0c35685ef7a1c71d12f074ef extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_3a4015731e33395e11474ee991f85e8e6ab547810e1ba04a925bfceda332e0fe = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_3a4015731e33395e11474ee991f85e8e6ab547810e1ba04a925bfceda332e0fe->enter($__internal_3a4015731e33395e11474ee991f85e8e6ab547810e1ba04a925bfceda332e0fe_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Twig/images/chevron-right.svg"));

        $__internal_ec127316ee22a9a83f8d3a4aaaaf4774f5651768725a439f8a43b1ea66b7d001 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_ec127316ee22a9a83f8d3a4aaaaf4774f5651768725a439f8a43b1ea66b7d001->enter($__internal_ec127316ee22a9a83f8d3a4aaaaf4774f5651768725a439f8a43b1ea66b7d001_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Twig/images/chevron-right.svg"));

        // line 1
        echo "<svg width=\"1792\" height=\"1792\" viewBox=\"0 0 1792 1792\" xmlns=\"http://www.w3.org/2000/svg\"><path fill=\"#FFF\" d=\"M1363 877l-742 742q-19 19-45 19t-45-19l-166-166q-19-19-19-45t19-45l531-531-531-531q-19-19-19-45t19-45l166-166q19-19 45-19t45 19l742 742q19 19 19 45t-19 45z\"/></svg>
";
        
        $__internal_3a4015731e33395e11474ee991f85e8e6ab547810e1ba04a925bfceda332e0fe->leave($__internal_3a4015731e33395e11474ee991f85e8e6ab547810e1ba04a925bfceda332e0fe_prof);

        
        $__internal_ec127316ee22a9a83f8d3a4aaaaf4774f5651768725a439f8a43b1ea66b7d001->leave($__internal_ec127316ee22a9a83f8d3a4aaaaf4774f5651768725a439f8a43b1ea66b7d001_prof);

    }

    public function getTemplateName()
    {
        return "@Twig/images/chevron-right.svg";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<svg width=\"1792\" height=\"1792\" viewBox=\"0 0 1792 1792\" xmlns=\"http://www.w3.org/2000/svg\"><path fill=\"#FFF\" d=\"M1363 877l-742 742q-19 19-45 19t-45-19l-166-166q-19-19-19-45t19-45l531-531-531-531q-19-19-19-45t19-45l166-166q19-19 45-19t45 19l742 742q19 19 19 45t-19 45z\"/></svg>
", "@Twig/images/chevron-right.svg", "C:\\xampp\\htdocs\\BancoErrores\\vendor\\symfony\\symfony\\src\\Symfony\\Bundle\\TwigBundle\\Resources\\views\\images\\chevron-right.svg");
    }
}
