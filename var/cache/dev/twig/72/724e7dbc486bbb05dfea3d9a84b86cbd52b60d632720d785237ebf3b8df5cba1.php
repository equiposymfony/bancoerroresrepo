<?php

/* users/index.html.twig */
class __TwigTemplate_0af3b2992cda03088202539eaf424ecf67737cdc453fff6adce9a759c091d999 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("base.html.twig", "users/index.html.twig", 1);
        $this->blocks = array(
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_d6f45d5fbc1baaaaad8ac067a96d5fa53fd92de5b4eb80aea36b39d148a29712 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_d6f45d5fbc1baaaaad8ac067a96d5fa53fd92de5b4eb80aea36b39d148a29712->enter($__internal_d6f45d5fbc1baaaaad8ac067a96d5fa53fd92de5b4eb80aea36b39d148a29712_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "users/index.html.twig"));

        $__internal_165bcf7126f48a9049a9a6d77b4f5146f9a2b3c61a7dedc72943b0c84966c552 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_165bcf7126f48a9049a9a6d77b4f5146f9a2b3c61a7dedc72943b0c84966c552->enter($__internal_165bcf7126f48a9049a9a6d77b4f5146f9a2b3c61a7dedc72943b0c84966c552_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "users/index.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_d6f45d5fbc1baaaaad8ac067a96d5fa53fd92de5b4eb80aea36b39d148a29712->leave($__internal_d6f45d5fbc1baaaaad8ac067a96d5fa53fd92de5b4eb80aea36b39d148a29712_prof);

        
        $__internal_165bcf7126f48a9049a9a6d77b4f5146f9a2b3c61a7dedc72943b0c84966c552->leave($__internal_165bcf7126f48a9049a9a6d77b4f5146f9a2b3c61a7dedc72943b0c84966c552_prof);

    }

    // line 3
    public function block_body($context, array $blocks = array())
    {
        $__internal_36bb0f78a6999f74db571d2b11ce76cf3e0c1ddf663ac029c3d5c6d12a3e0b5a = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_36bb0f78a6999f74db571d2b11ce76cf3e0c1ddf663ac029c3d5c6d12a3e0b5a->enter($__internal_36bb0f78a6999f74db571d2b11ce76cf3e0c1ddf663ac029c3d5c6d12a3e0b5a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_9a75eb4bb0fa1b6c9b6c4eb5759170f6072536a28e38a645d9edc79f9f2aeb99 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_9a75eb4bb0fa1b6c9b6c4eb5759170f6072536a28e38a645d9edc79f9f2aeb99->enter($__internal_9a75eb4bb0fa1b6c9b6c4eb5759170f6072536a28e38a645d9edc79f9f2aeb99_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 4
        echo "    <h1>Users list</h1>

    <table>
        <thead>
            <tr>
                <th>Id</th>
                <th>Iduser</th>
                <th>Nameuser</th>
                <th>Actions</th>
            </tr>
        </thead>
        <tbody>
        ";
        // line 16
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["users"] ?? $this->getContext($context, "users")));
        foreach ($context['_seq'] as $context["_key"] => $context["user"]) {
            // line 17
            echo "            <tr>
                <td><a href=\"";
            // line 18
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("users_show", array("id" => $this->getAttribute($context["user"], "id", array()))), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, $this->getAttribute($context["user"], "id", array()), "html", null, true);
            echo "</a></td>
                <td>";
            // line 19
            echo twig_escape_filter($this->env, $this->getAttribute($context["user"], "idUser", array()), "html", null, true);
            echo "</td>
                <td>";
            // line 20
            echo twig_escape_filter($this->env, $this->getAttribute($context["user"], "nameUser", array()), "html", null, true);
            echo "</td>
                <td>
                    <ul>
                        <li>
                            <a href=\"";
            // line 24
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("users_show", array("id" => $this->getAttribute($context["user"], "id", array()))), "html", null, true);
            echo "\">show</a>
                        </li>
                        <li>
                            <a href=\"";
            // line 27
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("users_edit", array("id" => $this->getAttribute($context["user"], "id", array()))), "html", null, true);
            echo "\">edit</a>
                        </li>
                    </ul>
                </td>
            </tr>
        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['user'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 33
        echo "        </tbody>
    </table>

    <ul>
        <li>
            <a href=\"";
        // line 38
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("users_new");
        echo "\">Create a new user</a>
        </li>
    </ul>
";
        
        $__internal_9a75eb4bb0fa1b6c9b6c4eb5759170f6072536a28e38a645d9edc79f9f2aeb99->leave($__internal_9a75eb4bb0fa1b6c9b6c4eb5759170f6072536a28e38a645d9edc79f9f2aeb99_prof);

        
        $__internal_36bb0f78a6999f74db571d2b11ce76cf3e0c1ddf663ac029c3d5c6d12a3e0b5a->leave($__internal_36bb0f78a6999f74db571d2b11ce76cf3e0c1ddf663ac029c3d5c6d12a3e0b5a_prof);

    }

    public function getTemplateName()
    {
        return "users/index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  112 => 38,  105 => 33,  93 => 27,  87 => 24,  80 => 20,  76 => 19,  70 => 18,  67 => 17,  63 => 16,  49 => 4,  40 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends 'base.html.twig' %}

{% block body %}
    <h1>Users list</h1>

    <table>
        <thead>
            <tr>
                <th>Id</th>
                <th>Iduser</th>
                <th>Nameuser</th>
                <th>Actions</th>
            </tr>
        </thead>
        <tbody>
        {% for user in users %}
            <tr>
                <td><a href=\"{{ path('users_show', { 'id': user.id }) }}\">{{ user.id }}</a></td>
                <td>{{ user.idUser }}</td>
                <td>{{ user.nameUser }}</td>
                <td>
                    <ul>
                        <li>
                            <a href=\"{{ path('users_show', { 'id': user.id }) }}\">show</a>
                        </li>
                        <li>
                            <a href=\"{{ path('users_edit', { 'id': user.id }) }}\">edit</a>
                        </li>
                    </ul>
                </td>
            </tr>
        {% endfor %}
        </tbody>
    </table>

    <ul>
        <li>
            <a href=\"{{ path('users_new') }}\">Create a new user</a>
        </li>
    </ul>
{% endblock %}
", "users/index.html.twig", "C:\\xampp\\htdocs\\BancoErrores\\app\\Resources\\views\\users\\index.html.twig");
    }
}
