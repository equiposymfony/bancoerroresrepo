<?php

/* security/login.html.twig */
class __TwigTemplate_18848e5e006cae0e9f0a36aa89e13f40ff41fbfc58341bec3e9518996554190d extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("base.html.twig", "security/login.html.twig", 1);
        $this->blocks = array(
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_b90dcc50c78d01674e34f868c2a5133c6c18ac62168809a31397d261a6ac6e22 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_b90dcc50c78d01674e34f868c2a5133c6c18ac62168809a31397d261a6ac6e22->enter($__internal_b90dcc50c78d01674e34f868c2a5133c6c18ac62168809a31397d261a6ac6e22_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "security/login.html.twig"));

        $__internal_a0474738054bc280b20fabfa80cec0ae893c244984e90760bfdb3539fe2deca5 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_a0474738054bc280b20fabfa80cec0ae893c244984e90760bfdb3539fe2deca5->enter($__internal_a0474738054bc280b20fabfa80cec0ae893c244984e90760bfdb3539fe2deca5_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "security/login.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_b90dcc50c78d01674e34f868c2a5133c6c18ac62168809a31397d261a6ac6e22->leave($__internal_b90dcc50c78d01674e34f868c2a5133c6c18ac62168809a31397d261a6ac6e22_prof);

        
        $__internal_a0474738054bc280b20fabfa80cec0ae893c244984e90760bfdb3539fe2deca5->leave($__internal_a0474738054bc280b20fabfa80cec0ae893c244984e90760bfdb3539fe2deca5_prof);

    }

    // line 3
    public function block_body($context, array $blocks = array())
    {
        $__internal_bafab32bd84adb0a059b1b51602437cd9920832c4c9fb8ff3bbd8581d1c7abac = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_bafab32bd84adb0a059b1b51602437cd9920832c4c9fb8ff3bbd8581d1c7abac->enter($__internal_bafab32bd84adb0a059b1b51602437cd9920832c4c9fb8ff3bbd8581d1c7abac_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_670ce1d0aea334b0d64e398b8a0cad411bb9b85be59af0d4f004704ad4d496ca = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_670ce1d0aea334b0d64e398b8a0cad411bb9b85be59af0d4f004704ad4d496ca->enter($__internal_670ce1d0aea334b0d64e398b8a0cad411bb9b85be59af0d4f004704ad4d496ca_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 4
        echo "<div class=\"panel panel-success\">
    <div class=\"panel-heading\">
        <h3>Login</h3>
    </div>

    <div class=\"panel-body\">
        ";
        // line 10
        if (($context["error"] ?? $this->getContext($context, "error"))) {
            // line 11
            echo "            <div class=\"alert alert-danger\">";
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans($this->getAttribute(($context["error"] ?? $this->getContext($context, "error")), "messageKey", array()), $this->getAttribute(($context["error"] ?? $this->getContext($context, "error")), "messageData", array()), "security"), "html", null, true);
            echo "</div>
        ";
        }
        // line 13
        echo "        
        <form action=\"";
        // line 14
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("login");
        echo "\" method=\"post\">
        
        <div class=\"form-group\">
        <label for=\"username\">Username:</label>
            <input type=\"text\" id=\"username\" name=\"_username\" value=\"";
        // line 18
        echo twig_escape_filter($this->env, ($context["last_username"] ?? $this->getContext($context, "last_username")), "html", null, true);
        echo "\" class=\"form-control\" />
        </div>
        <div class=\"form-group\">

            <label for=\"password\">Password:</label>
            <input type=\"password\" id=\"password\" name=\"_password\" class=\"form-control\" />
        </div>
            ";
        // line 30
        echo "
            <button type=\"submit\" class=\"btn btn-info pull-right\">login</button>
        </form>
    </div>
</div>



";
        
        $__internal_670ce1d0aea334b0d64e398b8a0cad411bb9b85be59af0d4f004704ad4d496ca->leave($__internal_670ce1d0aea334b0d64e398b8a0cad411bb9b85be59af0d4f004704ad4d496ca_prof);

        
        $__internal_bafab32bd84adb0a059b1b51602437cd9920832c4c9fb8ff3bbd8581d1c7abac->leave($__internal_bafab32bd84adb0a059b1b51602437cd9920832c4c9fb8ff3bbd8581d1c7abac_prof);

    }

    public function getTemplateName()
    {
        return "security/login.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  85 => 30,  75 => 18,  68 => 14,  65 => 13,  59 => 11,  57 => 10,  49 => 4,  40 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends 'base.html.twig' %}

{% block body %}
<div class=\"panel panel-success\">
    <div class=\"panel-heading\">
        <h3>Login</h3>
    </div>

    <div class=\"panel-body\">
        {% if error %}
            <div class=\"alert alert-danger\">{{ error.messageKey|trans(error.messageData, 'security') }}</div>
        {% endif %}
        
        <form action=\"{{ path('login') }}\" method=\"post\">
        
        <div class=\"form-group\">
        <label for=\"username\">Username:</label>
            <input type=\"text\" id=\"username\" name=\"_username\" value=\"{{ last_username }}\" class=\"form-control\" />
        </div>
        <div class=\"form-group\">

            <label for=\"password\">Password:</label>
            <input type=\"password\" id=\"password\" name=\"_password\" class=\"form-control\" />
        </div>
            {#
                If you want to control the URL the user
                is redirected to on success (more details below)
                <input type=\"hidden\" name=\"_target_path\" value=\"/account\" />
            #}

            <button type=\"submit\" class=\"btn btn-info pull-right\">login</button>
        </form>
    </div>
</div>



{% endblock %}


", "security/login.html.twig", "C:\\xampp\\htdocs\\BancoErrores\\app\\Resources\\views\\security\\login.html.twig");
    }
}
