<?php

/* @Twig/images/icon-minus-square.svg */
class __TwigTemplate_c436a15a0929d5c2eb6a8328ce971b2e24d156f78ca75a0dae322ffeb31f0188 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_ffadf477841098a89a2cb796a7c55278c8bbaf65641bcb7bb769aae53ead7520 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_ffadf477841098a89a2cb796a7c55278c8bbaf65641bcb7bb769aae53ead7520->enter($__internal_ffadf477841098a89a2cb796a7c55278c8bbaf65641bcb7bb769aae53ead7520_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Twig/images/icon-minus-square.svg"));

        $__internal_f2772fb6e139390761624b19df356be90f976447cc9520149af27cf8d88ff6a0 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_f2772fb6e139390761624b19df356be90f976447cc9520149af27cf8d88ff6a0->enter($__internal_f2772fb6e139390761624b19df356be90f976447cc9520149af27cf8d88ff6a0_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Twig/images/icon-minus-square.svg"));

        // line 1
        echo "<svg width=\"1792\" height=\"1792\" viewBox=\"0 0 1792 1792\" xmlns=\"http://www.w3.org/2000/svg\"><path d=\"M1408 960V832q0-26-19-45t-45-19H448q-26 0-45 19t-19 45v128q0 26 19 45t45 19h896q26 0 45-19t19-45zm256-544v960q0 119-84.5 203.5T1376 1664H416q-119 0-203.5-84.5T128 1376V416q0-119 84.5-203.5T416 128h960q119 0 203.5 84.5T1664 416z\"/></svg>
";
        
        $__internal_ffadf477841098a89a2cb796a7c55278c8bbaf65641bcb7bb769aae53ead7520->leave($__internal_ffadf477841098a89a2cb796a7c55278c8bbaf65641bcb7bb769aae53ead7520_prof);

        
        $__internal_f2772fb6e139390761624b19df356be90f976447cc9520149af27cf8d88ff6a0->leave($__internal_f2772fb6e139390761624b19df356be90f976447cc9520149af27cf8d88ff6a0_prof);

    }

    public function getTemplateName()
    {
        return "@Twig/images/icon-minus-square.svg";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<svg width=\"1792\" height=\"1792\" viewBox=\"0 0 1792 1792\" xmlns=\"http://www.w3.org/2000/svg\"><path d=\"M1408 960V832q0-26-19-45t-45-19H448q-26 0-45 19t-19 45v128q0 26 19 45t45 19h896q26 0 45-19t19-45zm256-544v960q0 119-84.5 203.5T1376 1664H416q-119 0-203.5-84.5T128 1376V416q0-119 84.5-203.5T416 128h960q119 0 203.5 84.5T1664 416z\"/></svg>
", "@Twig/images/icon-minus-square.svg", "C:\\xampp\\htdocs\\BancoErrores\\vendor\\symfony\\symfony\\src\\Symfony\\Bundle\\TwigBundle\\Resources\\views\\images\\icon-minus-square.svg");
    }
}
