<?php

/* base.html.twig */
class __TwigTemplate_35a788ca461558bba9038e4c3f84ff08bc8c4a644fa3eb1893a42b9f45e0624c extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'stylesheets' => array($this, 'block_stylesheets'),
            'body' => array($this, 'block_body'),
            'javascripts' => array($this, 'block_javascripts'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_d8e4a61074807c8b517102f612a7b3c28e309b3c95338a4f870103732ab2aa07 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_d8e4a61074807c8b517102f612a7b3c28e309b3c95338a4f870103732ab2aa07->enter($__internal_d8e4a61074807c8b517102f612a7b3c28e309b3c95338a4f870103732ab2aa07_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "base.html.twig"));

        $__internal_daa2f9a3fedeb44461154c306adddcfc84005cf6436004629585978c68d93856 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_daa2f9a3fedeb44461154c306adddcfc84005cf6436004629585978c68d93856->enter($__internal_daa2f9a3fedeb44461154c306adddcfc84005cf6436004629585978c68d93856_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "base.html.twig"));

        // line 1
        echo "<!DOCTYPE html>
<html>
    <head>
        <meta charset=\"UTF-8\" />
        <title>";
        // line 5
        $this->displayBlock('title', $context, $blocks);
        echo "</title>
        ";
        // line 6
        $this->displayBlock('stylesheets', $context, $blocks);
        // line 7
        echo "        <link rel=\"icon\" type=\"image/x-icon\" href=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("favicon.ico"), "html", null, true);
        echo "\" />
        <link rel=\"stylesheet\" type=\"text/css\" href=\"https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css\">
        <link rel=\"stylesheet\" type=\"text/css\" href=\"https://cdnjs.cloudflare.com/ajax/libs/bootflat/2.0.4/css/bootflat.min.css\">
    </head>
    <body>
        <div class=\"container\">
            ";
        // line 13
        $this->displayBlock('body', $context, $blocks);
        // line 14
        echo "
        </div>
        
        ";
        // line 17
        $this->displayBlock('javascripts', $context, $blocks);
        // line 18
        echo "    </body>
</html>";
        
        $__internal_d8e4a61074807c8b517102f612a7b3c28e309b3c95338a4f870103732ab2aa07->leave($__internal_d8e4a61074807c8b517102f612a7b3c28e309b3c95338a4f870103732ab2aa07_prof);

        
        $__internal_daa2f9a3fedeb44461154c306adddcfc84005cf6436004629585978c68d93856->leave($__internal_daa2f9a3fedeb44461154c306adddcfc84005cf6436004629585978c68d93856_prof);

    }

    // line 5
    public function block_title($context, array $blocks = array())
    {
        $__internal_b30fc1df6693d4d772d4c0bd4d49efcfbd61853749c81d1a13b2e48edf68ed36 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_b30fc1df6693d4d772d4c0bd4d49efcfbd61853749c81d1a13b2e48edf68ed36->enter($__internal_b30fc1df6693d4d772d4c0bd4d49efcfbd61853749c81d1a13b2e48edf68ed36_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        $__internal_0eb5621ce3951bf8219eb10222b55fe40c9f22a331a1105e14fd73922961370c = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_0eb5621ce3951bf8219eb10222b55fe40c9f22a331a1105e14fd73922961370c->enter($__internal_0eb5621ce3951bf8219eb10222b55fe40c9f22a331a1105e14fd73922961370c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        echo "Welcome!";
        
        $__internal_0eb5621ce3951bf8219eb10222b55fe40c9f22a331a1105e14fd73922961370c->leave($__internal_0eb5621ce3951bf8219eb10222b55fe40c9f22a331a1105e14fd73922961370c_prof);

        
        $__internal_b30fc1df6693d4d772d4c0bd4d49efcfbd61853749c81d1a13b2e48edf68ed36->leave($__internal_b30fc1df6693d4d772d4c0bd4d49efcfbd61853749c81d1a13b2e48edf68ed36_prof);

    }

    // line 6
    public function block_stylesheets($context, array $blocks = array())
    {
        $__internal_2ccd2d5d24a50863cea78490e6a8705ec698c351dca72607b8190f66e682987a = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_2ccd2d5d24a50863cea78490e6a8705ec698c351dca72607b8190f66e682987a->enter($__internal_2ccd2d5d24a50863cea78490e6a8705ec698c351dca72607b8190f66e682987a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "stylesheets"));

        $__internal_60cf1532850c67524209537b25a3b8cd3a5b86316d4eeced9e18dfdedf860fdb = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_60cf1532850c67524209537b25a3b8cd3a5b86316d4eeced9e18dfdedf860fdb->enter($__internal_60cf1532850c67524209537b25a3b8cd3a5b86316d4eeced9e18dfdedf860fdb_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "stylesheets"));

        
        $__internal_60cf1532850c67524209537b25a3b8cd3a5b86316d4eeced9e18dfdedf860fdb->leave($__internal_60cf1532850c67524209537b25a3b8cd3a5b86316d4eeced9e18dfdedf860fdb_prof);

        
        $__internal_2ccd2d5d24a50863cea78490e6a8705ec698c351dca72607b8190f66e682987a->leave($__internal_2ccd2d5d24a50863cea78490e6a8705ec698c351dca72607b8190f66e682987a_prof);

    }

    // line 13
    public function block_body($context, array $blocks = array())
    {
        $__internal_1becdaef7483f0104bb7a6b349d926ee97d65a51de5b2832037f3959d5f2a37c = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_1becdaef7483f0104bb7a6b349d926ee97d65a51de5b2832037f3959d5f2a37c->enter($__internal_1becdaef7483f0104bb7a6b349d926ee97d65a51de5b2832037f3959d5f2a37c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_5b227e3bc2cc70b29790c026ef1eb4796e466ca38b79b1b1fd4ec8ea6531efb3 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_5b227e3bc2cc70b29790c026ef1eb4796e466ca38b79b1b1fd4ec8ea6531efb3->enter($__internal_5b227e3bc2cc70b29790c026ef1eb4796e466ca38b79b1b1fd4ec8ea6531efb3_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        
        $__internal_5b227e3bc2cc70b29790c026ef1eb4796e466ca38b79b1b1fd4ec8ea6531efb3->leave($__internal_5b227e3bc2cc70b29790c026ef1eb4796e466ca38b79b1b1fd4ec8ea6531efb3_prof);

        
        $__internal_1becdaef7483f0104bb7a6b349d926ee97d65a51de5b2832037f3959d5f2a37c->leave($__internal_1becdaef7483f0104bb7a6b349d926ee97d65a51de5b2832037f3959d5f2a37c_prof);

    }

    // line 17
    public function block_javascripts($context, array $blocks = array())
    {
        $__internal_8006fdf71236b9c40e601a5b8a80851f7bb4a30721eeba13b03bca0c465ad6bb = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_8006fdf71236b9c40e601a5b8a80851f7bb4a30721eeba13b03bca0c465ad6bb->enter($__internal_8006fdf71236b9c40e601a5b8a80851f7bb4a30721eeba13b03bca0c465ad6bb_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "javascripts"));

        $__internal_bdb6fa9715d4eb67dbbc3153eaa8f87a252488dcd568ca5b786d516c0fcc218a = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_bdb6fa9715d4eb67dbbc3153eaa8f87a252488dcd568ca5b786d516c0fcc218a->enter($__internal_bdb6fa9715d4eb67dbbc3153eaa8f87a252488dcd568ca5b786d516c0fcc218a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "javascripts"));

        
        $__internal_bdb6fa9715d4eb67dbbc3153eaa8f87a252488dcd568ca5b786d516c0fcc218a->leave($__internal_bdb6fa9715d4eb67dbbc3153eaa8f87a252488dcd568ca5b786d516c0fcc218a_prof);

        
        $__internal_8006fdf71236b9c40e601a5b8a80851f7bb4a30721eeba13b03bca0c465ad6bb->leave($__internal_8006fdf71236b9c40e601a5b8a80851f7bb4a30721eeba13b03bca0c465ad6bb_prof);

    }

    public function getTemplateName()
    {
        return "base.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  123 => 17,  106 => 13,  89 => 6,  71 => 5,  60 => 18,  58 => 17,  53 => 14,  51 => 13,  41 => 7,  39 => 6,  35 => 5,  29 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<!DOCTYPE html>
<html>
    <head>
        <meta charset=\"UTF-8\" />
        <title>{% block title %}Welcome!{% endblock %}</title>
        {% block stylesheets %}{% endblock %}
        <link rel=\"icon\" type=\"image/x-icon\" href=\"{{ asset('favicon.ico') }}\" />
        <link rel=\"stylesheet\" type=\"text/css\" href=\"https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css\">
        <link rel=\"stylesheet\" type=\"text/css\" href=\"https://cdnjs.cloudflare.com/ajax/libs/bootflat/2.0.4/css/bootflat.min.css\">
    </head>
    <body>
        <div class=\"container\">
            {% block body %}{% endblock %}

        </div>
        
        {% block javascripts %}{% endblock %}
    </body>
</html>", "base.html.twig", "C:\\xampp\\htdocs\\BancoErrores\\app\\Resources\\views\\base.html.twig");
    }
}
